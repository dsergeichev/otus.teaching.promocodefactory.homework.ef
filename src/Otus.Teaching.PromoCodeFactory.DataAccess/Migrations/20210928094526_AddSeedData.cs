﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO Roles(Id, Name, Description) 
VALUES  ('53729686-A368-4EEB-8BFA-CC69B6050D02', 'Admin', 'Администратор'),
        ('B0AE7AAC-5493-45CD-AD16-87426A5E7665', 'PartnerManager', 'Партнерский менеджер');

INSERT INTO Employees(Id, Email, FirstName, LastName, RoleId, AppliedPromocodesCount) 
VALUES  ('451533D5-D8D5-4A11-9C7B-EB9F14E1A32F', 'owner@somemail.ru', 'Иван', 'Сергеев', '53729686-A368-4EEB-8BFA-CC69B6050D02', 5),
        ('F766E2BF-340A-46EA-BFF3-F1700B435895', 'andreev@somemail.ru', 'Петр', 'Андреев', 'B0AE7AAC-5493-45CD-AD16-87426A5E7665', 10);

INSERT INTO Preferences(Id, Name)
VALUES  ('EF7F299F-92D7-459F-896E-078ED53EF99C', 'Театр'),
        ('C4BDA62E-FC74-4256-A956-4760B3858CBD', 'Семья'),
        ('EBEDD1CF-B245-4865-B077-879E23B989A1', 'Дети');

INSERT INTO Customers(Id, Email, FirstName, LastName) 
VALUES  ('9DD5775C-9553-4CDE-9E2D-DF3AB44EE57C', 'ivan_sergeev@mail.ru', 'Иван', 'Петров');

INSERT INTO CustomerPreference(CustomerId, PreferenceId) 
VALUES  ('9DD5775C-9553-4CDE-9E2D-DF3AB44EE57C', 'C4BDA62E-FC74-4256-A956-4760B3858CBD'),
        ('9DD5775C-9553-4CDE-9E2D-DF3AB44EE57C', 'EBEDD1CF-B245-4865-B077-879E23B989A1');"
);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
