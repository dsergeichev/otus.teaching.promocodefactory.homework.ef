﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить клиента вместе с выданными ему промомкодами
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences?.Select(x => new PreferenceResponse
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToList(),
            };
            return customerModel;
        }

        /// <summary>
        /// Создать нового клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = new List<Preference>();
            foreach (var id in request.PreferenceIds)
            {
                var preference = await _preferenceRepository.GetByIdAsync(id);
                if (preference == null)
                {
                    return NotFound();
                }
                preferences.Add(preference);
            }
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Count == 0 ? null : preferences,
            };
            return Ok(customer);
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var foundCustomer = await _customerRepository.GetByIdAsync(id);
            if (foundCustomer == null)
            {
                return NotFound();
            }

            // Не стал тут оптимизировать, чтобы уже существующие предпочтения не искать повторно,
            // т.к. их недолжно быть много (дестяков тысяч не будет).
            var preferences = new List<Preference>();
            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = await _preferenceRepository.GetByIdAsync(preferenceId);
                if (preference == null)
                {
                    return NotFound();
                }
                preferences.Add(preference);
            }

            foundCustomer.FirstName = request.FirstName;
            foundCustomer.LastName = request.LastName;
            foundCustomer.Email = request.Email;
            foundCustomer.Preferences = preferences.Count == 0 ? null : preferences;

            await _customerRepository.UpdateAsync(foundCustomer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промокодами
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.DeleteAsync(id);
            return Ok();
        }
    }
}