﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();

            var promocodesModelList = promocodes.Select(x =>
                new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString("yyyy-MM-ddTHH:mm:ss:fffZ"),
                    EndDate = x.EndDate.ToString("yyyy-MM-ddTHH:mm:ss:fffZ"),
                    PartnerName = x.PartnerName,
                }).ToList();

            return promocodesModelList;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customers = await _customerRepository.GetAllAsync();
            customers = customers.
                Where(x => x.Preferences != null && x.Preferences.
                Any(p => p.Name == request.Preference));
            if (!customers.Any())
            {
                return NotFound();
            }
            var preference = customers.First().Preferences.FirstOrDefault(x => x.Name == request.Preference);

            foreach (var customer in customers)
            {
                var promoCode = new PromoCode
                {
                    ServiceInfo = request.ServiceInfo,
                    Preference = preference,
                    BeginDate = DateTime.Now,
                    Code = request.PromoCode,
                    EndDate = DateTime.Now.AddHours(48), // TODO считывать из конфига или бд время жизни промокода
                    PartnerName = request.PartnerName,
                };
                promoCode = await _promoCodeRepository.AddAsync(promoCode);

                var customerPromoCodes = customer.PromoCodes.ToList();
                if (customerPromoCodes == null)
                {
                    customer.PromoCodes = new List<PromoCode>();
                }
                customerPromoCodes.Add(promoCode);
                customer.PromoCodes = customerPromoCodes;
                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}